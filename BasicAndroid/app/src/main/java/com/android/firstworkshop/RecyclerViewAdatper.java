package com.android.firstworkshop;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class RecyclerViewAdatper extends RecyclerView.Adapter<RecyclerViewAdatper.RecyclerViewHolder> {

    List<String> data;
    List<Integer> dataPrices;
    public RecyclerViewAdatper(List<String> data,List<Integer> dataPrices)
    {
        this.data=data;
        this.dataPrices=dataPrices;
    }
    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View recyclerRow =layoutInflater.inflate(R.layout.recycler_item,parent,false);
        return new RecyclerViewHolder(recyclerRow);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        String carMake=data.get(position);
        int carPrice=dataPrices.get(position);
        holder.carMakeTextView.setText(carMake);
        holder.carPriceTextView.setText("Price: "+carPrice+"$");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{

        final TextView carMakeTextView;
        final TextView carPriceTextView;
        final Button btnBuy;
       public RecyclerViewHolder(View itemView) {
           super(itemView);
           this.carMakeTextView= itemView.findViewById(R.id.car_make);
           this.carPriceTextView=itemView.findViewById(R.id.car_price);
           this.btnBuy= itemView.findViewById(R.id.buy_btn);
       }
   }
}
