package com.android.firstworkshop;

import java.util.ArrayList;
import java.util.List;

public class DataSource {
    public List<String> getCars(){
        List<String> cars = new ArrayList<>();
        cars.add("BMW");
        cars.add("Audi");
        cars.add("Tesla");
        cars.add("VW");
        cars.add("Mini");
        cars.add("Lada");
        cars.add("Ford");
        cars.add("Ferrari");
        cars.add("Alfa");
        cars.add("Lambo");
        cars.add("Masserati");
        cars.add("Citroen");
        cars.add("Dacia");
        cars.add("Audi");
        cars.add("Porche");
        return cars;
    }
    public List<Integer> getCarsPrice(){
        List<Integer> prices = new ArrayList<>();
        prices.add(123);
        prices.add(634);
        prices.add(654);
        prices.add(346);
        prices.add(346);
        prices.add(346);
        prices.add(346);
        prices.add(346);
        prices.add(346);
        prices.add(346);
        prices.add(346);
        prices.add(346);
        prices.add(346);
        prices.add(346);
        prices.add(346);
        return prices;
    }
}
