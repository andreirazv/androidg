package com.android.firstworkshop;

import java.util.ArrayList;
import java.util.List;

public class SpinnerDataSource {


    public List<String> getDataSource()
    {
        List<String> androids = new ArrayList<>();
        androids.add("kitkat");
        androids.add("oreo");
        androids.add("pie");
        androids.add("JellyBean");
        return androids;
    }
}
